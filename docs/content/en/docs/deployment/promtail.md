---
title: "Promtail"
linkTitle: "Promtail"
weight: 5
description: >
  Configuring Promtail.
---

Promtail is configured in a YAML file (usually referred to as config.yaml) which contains information on the Promtail server, where positions are stored, and how to scrape logs from files. See `deployments/promtail/watchdog.yaml`